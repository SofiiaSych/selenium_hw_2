import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

public class GoogleSearchTest {

    @Test
    public void SearchWordTest() {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.get("https://www.google.com/");

        WebElement element = driver.findElement(By.name("q"));
        element.sendKeys("Apple");
        element.submit();

        String title = driver.getTitle();
        Assert.assertTrue(title.contains("Apple"), "Title doesn't contain word 'Apple'");

        WebElement imageBtn = driver.findElement(By.xpath("//a[contains(text(), 'Зображення')]"));
        imageBtn.click();

        WebElement img = driver.findElement(By.cssSelector("*[class = 'rQEFy NZmxZe']"));
        Assert.assertEquals(img.getText(), "Зображення");
    }
}
