import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class GmailTest {

    private String EMAIL = "sychsofiiatest@gmail.com";
    private String PASSWORD = "sofiia_test1";
    private WebDriver driver;

    @Test
    public void VerifySendingLetterTest(){
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("https://accounts.google.com/signin/v2/identifier?continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&service=mail&sacu=1&rip=1&flowName=GlifWebSignIn&flowEntry=ServiceLogin");

        WebElement emailInput = driver.findElement(By.xpath("//input[@type='email']"));
        emailInput.sendKeys(EMAIL);

        WebElement submitEmailBtn = driver.findElement(By.cssSelector("*[class='VfPpkd-LgbsSe VfPpkd-LgbsSe-OWXEXe-k8QpJ VfPpkd-LgbsSe-OWXEXe-dgl2Hf nCP5yc AjY5Oe DuMIQc qIypjc TrZEUc']"));
        submitEmailBtn.click();

        WebElement passwordInput = driver.findElement(By.xpath("//input[@name = 'password']"));
        passwordInput.sendKeys(PASSWORD);

        WebElement submitPasswordBtn  = driver.findElement(By.cssSelector("*[class='VfPpkd-LgbsSe VfPpkd-LgbsSe-OWXEXe-k8QpJ VfPpkd-LgbsSe-OWXEXe-dgl2Hf nCP5yc AjY5Oe DuMIQc qIypjc TrZEUc']"));
        submitPasswordBtn.click();

        WebElement writeBtn = driver.findElement(By.xpath("//*[@class='z0']//div"));
        writeBtn.click();

        WebElement senderTexArea = driver.findElement(By.cssSelector("div.wO.nr.l1 > textarea"));
        senderTexArea.sendKeys("sych_sofia@i.ua");

        WebElement themeInput = driver.findElement(By.cssSelector("input.aoT[name='subjectbox']"));
        themeInput.sendKeys("Test letter");

        WebElement bodyMessageInput = driver.findElement(By.id(":9l"));
        bodyMessageInput.sendKeys("Some text");

        WebElement sendBtn = driver.findElement(By.id(":86"));
        sendBtn.click();

        WebElement popUp = driver.findElement(By.cssSelector(".vh > .aT"));
        Assert.assertTrue(popUp.isDisplayed(), "PopUp doesn't appear");
    }

    @AfterMethod
    public void cleanUp() {
        driver.quit();
    }
}

